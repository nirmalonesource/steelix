//
//  ProductwiseViewController.swift
//  STEELIX
//
//  Created by My Mac on 09/03/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import iOSDropDown
import UIKit
import Alamofire
import SVProgressHUD


class ProductwiseTables:UITableViewCell{
    
    
    @IBOutlet weak var txtname: UITextField!
    @IBOutlet weak var txtdate: UITextField!
    
    @IBOutlet weak var txtorderno: UITextField!
    @IBOutlet weak var txtqunt: UITextField!
    
}


class ProductwiseViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    var disids:String = ""
    var ProductID:Int = 0
    var disid :Int=0
    var Did:String = ""
    var authorization = ""
    var arr:NSArray = []
    @IBOutlet weak var txtproduct: DropDown!
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var todate: UITextField!
    @IBOutlet weak var frmdate: UITextField!
    
    
    
    
    let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showDatePicker()
        Did =  UserDefaults.standard.value(forKey:"id") as! String
        disid = UserDefaults.standard.value(forKey:"ID") as! Int
        authorization = UserDefaults.standard.string(forKey:"Token") ?? ""
        disids = String(disid)
        
        productwise()
        ProductList()
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ProductwiseTables = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProductwiseTables
        
        let orderno =  (arr[indexPath.row] as AnyObject).value(forKey: "OrderID")
        let date = (arr[indexPath.row] as AnyObject).value(forKey: "OrderDate")
        let quantity = (arr[indexPath.row] as AnyObject).value(forKey: "totalQty")
        let Fname = (arr[indexPath.row] as AnyObject).value(forKey: "FirstName")
        let Lname = (arr[indexPath.row] as AnyObject).value(forKey: "LastName")
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = " dd MMM,yyyy"
        
        let date1 = dateFormatterGet.date(from: date as! String)
        
        cell.txtdate.text=dateFormatterPrint.string(from: date1!)
        
        
        cell.txtqunt.text = quantity as? String
        cell.txtorderno.text =  orderno as? String
        cell.txtname.text = Fname as? String
        
        
        return cell
    }
    

    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action:#selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.bordered, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        
        todate.inputAccessoryView = toolbar
        todate.inputView = datePicker
        
        frmdate.inputAccessoryView = toolbar
        frmdate.inputView = datePicker
    }
    
    @objc  func donedatePicker(){
        //For date formate
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        
        if  todate.isEditing == true{
            todate.text = formatter.string(from:datePicker.date)
            
            self.view.endEditing(true)
            
        }
        
        if frmdate.isEditing == true{
            frmdate.text = formatter.string(from:datePicker.date)
            //dismiss date picker dialog
            self.view.endEditing(true)
            
        }
    }
    @IBAction func btnback(_ sender: Any) {
           let second:DistributorMainDashboardViewController = self.storyboard?.instantiateViewController(withIdentifier: "DistributorMainDashboardViewController") as! DistributorMainDashboardViewController
          self.navigationController?.pushViewController(second, animated:true)
    }
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
    
    func productwise()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            //            let headers = ["Authorization": "Basic \(base64LoginData)",
            //                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
            //                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
            //                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            let headers = [
                
                "Authorization": "bearer "  +  authorization
            ]
            let parameter = [
                "DistributorID":disids,
                "from": frmdate.text!,
                "to" :todate.text!,
                "DealerID" :ProductID
                ] as [String : Any]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.productwise)!,
                              method: .post,
                              parameters: parameter,
                              headers: headers as! HTTPHeaders).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        
                        
                        guard let data = response.result.value as? [String:AnyObject],
                            
                            
                            
                            
                            let _ = data["status"]! as? Bool
                            else{
                                
                                
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        // self.subjectList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            let dic =  resData["data"]
                            self.arr  = dic!["report_list"] as! NSArray
                            
                            self.tblView.reloadData()
                            //    let dic1 = dic?.value(forKey:"access_token")
                            
                        }
                            
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                        self.view.layoutIfNeeded()
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    func ProductList()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            //            let headers = ["Authorization": "Basic \(base64LoginData)",
            //                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
            //                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
            //                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            //            let parameter = [
            //                "mobileno":mobileno!,
            //                "deviceid":UIDevice.current.identifierForVendor!.uuidString,
            //                "otp":OTP
            //                ] as [String : Any]
            let headers = ["Authorization":"bearer " + authorization]
            
        
            
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.ProductList)!,
                              method: .get,
                              parameters: nil,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        // self.subjectList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            
                            let dic:NSDictionary = resData["data"] as! NSDictionary
                            let dic1:NSArray = dic["productlist"] as! NSArray
                            
                            
                            self.txtproduct.optionArray = dic1.value(forKey:"ProductName") as! [String]
                            
                            self.txtproduct.didSelect{(selectedText,index,id) in
                                self.txtproduct.text = "Selected String: \(selectedText) \n index: \(index)"
                                
                                let dic2 = dic1[index]
                                dicN = dic2 as! NSDictionary
                                
                                let dic3 = dic1.value(forKey: "ProductID")
                                
                                
                                self.txtproduct.text = (dic2 as AnyObject).value(forKey:"ProductName") as! String
                                self.ProductID = Int((dic3 as AnyObject).object(at:index) as! String)!
                                print(self.ProductID)
                                self.productwise()
                                
                              
                            }
                            
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                            print("invalid Otp")
                        }
                        
                        self.view.layoutIfNeeded()
                        
                        SVProgressHUD.dismiss()
                    case.failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    @IBAction func btndealerwise(_ sender: Any) {
        
        let second:ReportViewController = self.storyboard?.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
        self.navigationController?.pushViewController(second, animated:true)
    }
}
