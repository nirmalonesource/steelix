//
//  EventSelectedViewController.swift
//  STEELIX
//
//  Created by My Mac on 13/03/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit
class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    
}



class EventSelectedViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    var s:String = ""
    
    var eventselectedic:NSDictionary = [:]
    var eventgallary:NSArray = []
 override func viewDidLoad() {
        super.viewDidLoad()
          eventgallary = eventselectedic.value(forKey: "event_gallary") as! NSArray
       
    }
   
    @IBAction func btnback(_ sender: Any) {
               let second:EventViewController = self.storyboard?.instantiateViewController(withIdentifier: "EventViewController") as!
               EventViewController
                self.navigationController?.pushViewController(second, animated:true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return eventgallary.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell=collectionView.dequeueReusableCell(withReuseIdentifier:"cell", for:indexPath) as! CollectionViewCell
        
        eventgallary = eventselectedic.value(forKey: "event_gallary") as! NSArray
        print(eventgallary)
        s = (((eventgallary[indexPath.item] as AnyObject).value(forKey: "ImageURL")) as! NSString) as String
        
        print(s)
        let url = NSURL(string:s)
        let data = NSData(contentsOf:url! as URL)
        cell.img.image = UIImage(data:data! as Data)
        return cell
    }
    

 

}
