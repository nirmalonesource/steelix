

import UIKit
import Alamofire
import SVProgressHUD.SVIndefiniteAnimatedView



class TimeCell: UICollectionViewCell {
    var viewController: UIViewController?
    @IBOutlet weak var imgView: UIImageView!
    
}
class TheaterCell: UITableViewCell {

    @IBOutlet weak var ttxteventname: UITextField!
    var eventgallary:NSArray = []
    var eventlist:NSArray = []
   
    @IBOutlet weak var txteventname: UITextField!
    
    var s:String = ""
    @IBOutlet var showtimesCollection: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        showtimesCollection.delegate = self
        showtimesCollection.dataSource = self
        txteventname.roundedRight()
//        self.showtimesCollection.register(TheaterCell.self as AnyClass, forCellWithReuseIdentifier: "Cell")
        
        
        
    }
    func reloadCollectionView() -> Void {
        self.showtimesCollection.reloadData()
        
    }
}

extension TheaterCell: UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var dic :NSDictionary = eventlist.object(at: showtimesCollection.tag) as! NSDictionary
     let navController = self.window!.rootViewController as! UINavigationController
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let second = mainStoryboard.instantiateViewController(withIdentifier: "EventSelectedViewController") as! EventSelectedViewController
        second.eventselectedic = dic
    navController.pushViewController(second,animated:true)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return eventgallary.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = showtimesCollection.dequeueReusableCell(withReuseIdentifier: "timeCell", for: indexPath) as! TimeCell
        s = (((eventgallary[indexPath.item] as AnyObject).value(forKey: "ImageURL")) as! NSString) as String
        let url = NSURL(string:s)
        let data = NSData(contentsOf:url! as URL)
        cell.imgView.image = UIImage(data:data! as Data)
        return cell
    }
}
class EventViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var authorization :String = ""
    var eventlist:NSArray = []
    var arr1:NSArray = []
    var id :String = ""
 

    
    @IBOutlet weak var topview: UIView!
    
    
    @IBOutlet var tbl: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let theaterCell:TheaterCell = tableView.dequeueReusableCell(withIdentifier: "TheaterCell", for: indexPath) as! TheaterCell
        theaterCell.eventgallary = ((eventlist.object(at: indexPath.row)) as AnyObject).value(forKey: "event_gallary") as! NSArray
        theaterCell.ttxteventname.text = (eventlist[indexPath.row] as AnyObject).value(forKey:"EventDescription") as! String
         theaterCell.eventlist = eventlist
        theaterCell.showtimesCollection.tag = indexPath.row
        theaterCell.reloadCollectionView()
        return theaterCell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
       
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topview.roundedBottom()
        id = UserDefaults.standard.string(forKey: "Roleid")!
        authorization = UserDefaults.standard.string(forKey:"Token") ?? ""
        if let data = UserDefaults.standard.data(forKey: "EVENTARRAY"),
            let myPeopleList = NSKeyedUnarchiver.unarchiveObject(with: data)  {
            eventlist = myPeopleList as! NSArray
            arr1 = arr1.value(forKey:"event_gallary") as! NSArray
            
        eventwise()
        }
        else {
            print("There is an issue")
        }
        
    
        
    }

    @IBAction func btnback(_ sender: Any) {
        if id == "2"{
       let second:DistributorMainDashboardViewController = self.storyboard?.instantiateViewController(withIdentifier: "DistributorMainDashboardViewController") as!
      DistributorMainDashboardViewController
      self.navigationController?.pushViewController(second, animated:true)

    }
        else{
            let second:DashBoardMainViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardMainViewController") as!
            DashBoardMainViewController
            self.navigationController?.pushViewController(second, animated:true)

        }
   
    }
    func eventwise()
{
    if !isInternetAvailable(){
        noInternetConnectionAlert(uiview:self)
    }

    else {

        SVProgressHUD.show(withStatus: nil)

        let username = ServiceList.USERNAME
        let password = ServiceList.PASSWORD
        let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
        let base64LoginData = loginData.base64EncodedString()
      
        let headers = ["Authorization":"bearer " + authorization]

        Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.eventwise)!,
                          method: .post,
                          parameters: nil,
                          headers: headers).responseJSON
            { (response:DataResponse) in
                switch(response.result) {
                case .success(let data):
                    print(" i got my Data Yup..",data)

                    var resData : [String : AnyObject] = [:]
                    guard let data = response.result.value as? [String:AnyObject],

                        let _ = data["status"]! as? Bool
                        else{
                            print("Malformed data received from fetchAllRooms service")
                            SVProgressHUD.dismiss()

                            return
                    }

                    resData = data
                    // self.subjectList.removeAll()
                    if resData["status"] as? Bool ?? false
                    {
                        let dic =  resData["data"]
                        let arr1  = dic!["event_list"] as! NSArray
                        self.eventlist = arr1.value(forKey: "event_gallary") as! NSArray
                   
                        
                        let encodedData = NSKeyedArchiver.archivedData(withRootObject: arr1)
                        UserDefaults.standard.set(encodedData, forKey: "EVENTARRAY")
                      //  self.tbl.reloadData()
                    }
                    else
                    {
                        showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        print("invalid Otp")
                    }

                    self.view.layoutIfNeeded()

                    SVProgressHUD.dismiss()
                case.failure(let error):
                    print(error)
                    SVProgressHUD.dismiss()
                }
        }
    }
}
}
