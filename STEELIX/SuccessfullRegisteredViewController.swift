//
//  SuccessfullRegisteredViewController.swift
//  STEELIX
//
//  Created by My Mac on 30/02/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit

class SuccessfullRegisteredViewController: UIViewController {
  
   
    var Roleid:String = ""
    @IBAction func btnBack(_ sender: Any) {
         
        let second:OTPViewController = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
        self.navigationController?.pushViewController(second, animated:true)
    }
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var MiddleView: UIView!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var img1: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnNext.roundedAllCorner()
        MiddleView.layer.cornerRadius = 30
        MiddleView.layer.borderWidth = 0.5
        MiddleView.layer.borderColor = UIColor.black.cgColor
        Roleid = UserDefaults.standard.string(forKey:"Roleid")!
        
    topView.layer.cornerRadius = 30
    
        img1?.layer.cornerRadius = (img1?.frame.size.width ?? 0.0) / 2
        img1?.clipsToBounds = true
        img1?.layer.borderWidth = 3.0
        img1?.layer.borderColor = UIColor.white.cgColor
        // Do any additional setup after loading the view.
    }


    @IBAction func btnNExt(_ sender: Any) {
        
        if Roleid == "3"{
            
            let second:DashBoardMainViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardMainViewController") as! DashBoardMainViewController
            
            
            self.navigationController?.pushViewController(second, animated:true)
        }
        else{
            
            let second:DistributorMainDashboardViewController = self.storyboard?.instantiateViewController(withIdentifier: "DistributorMainDashboardViewController") as!
              DistributorMainDashboardViewController
            self.navigationController?.pushViewController(second, animated:true)
            
           
        }
    }
    
}
