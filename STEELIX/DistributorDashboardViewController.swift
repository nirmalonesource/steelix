//
//  DistributorDashboardViewController.swift
//  STEELIX
//
//  Created by My Mac on 02/03/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class DistributorDashboardTableCell:UITableViewCell{
    
    @IBOutlet weak var iconimg: UIImageView!
    @IBOutlet weak var txtCategory: UILabel!
    
}

class DistributorDashboardViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var authorization = ""
    var disid:Int = 0
    var disids = ""
    var AccessDisToken:String!
    @IBOutlet weak var txtmobile: UITextField!
    @IBOutlet weak var profileimg1: UIImageView!
    @IBOutlet weak var txtemail: UITextField!
    @IBOutlet weak var view1: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        disid = UserDefaults.standard.value(forKey:"ID") as! Int
        
        disids = String(disid)
        authorization =  UserDefaults.standard.string(forKey:"Token") ?? ""
        view1.roundedLeft()
        profileimg1?.layer.cornerRadius = (profileimg1?.frame.size.width ?? 0.0) / 2
        profileimg1?.clipsToBounds = true
        profileimg1?.layer.borderWidth = 3.0
        profileimg1?.layer.borderColor = UIColor.white.cgColor
    }
    
    let data=["DashBoard","Support","Terms & Condition","Report"]
     let images:[UIImage]=[
        UIImage(named:"Dashboard")!,
        UIImage(named:"Support")!,
        UIImage(named: "Terms & condition")!,
        UIImage(named:"Report")!
    ]
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let second:DistributorMainDashboardViewController = self.storyboard?.instantiateViewController(withIdentifier: "DistributorMainDashboardViewController") as! DistributorMainDashboardViewController
            self.navigationController?.pushViewController(second, animated:true)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        //        tableView.roundedRight()
        
        
        let cell:DistributorDashboardTableCell=tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! DistributorDashboardTableCell
            cell.txtCategory.text = data[indexPath.row]
            cell.iconimg?.image = images[indexPath.row]
            return cell
        
    }
    
  
    @IBAction func btnlogout(_ sender: Any) {
        logout()
    }
    
    @IBAction func btnedit(_ sender: Any) {
        
        let second:EditViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditViewController") as! EditViewController
             self.navigationController?.pushViewController(second, animated:true)
    }
    func logout()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            //            let headers = ["Authorization": "Basic \(base64LoginData)",
            //                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
            //                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
            //                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            let headers = [
                
                "Authorization": "bearer "  +  authorization
            ]
            let parameter = [
                "DistributorID":disids
                ]as [String : Any]
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.logout)!,
                              method: .post,
                              parameters: parameter,
                              headers: headers as! HTTPHeaders).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        
                        
                        guard let data = response.result.value as? [String:AnyObject],
                            
                            
                            
                            
                            let _ = data["status"]! as? Bool
                            else{
                                
                                
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        // self.subjectList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            let dic =  resData["data"]
                            let second:ViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            self.navigationController?.pushViewController(second, animated:true)
                            
                        }
                            
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                        self.view.layoutIfNeeded()
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
}
}
