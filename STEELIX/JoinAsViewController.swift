//
//  JoinAsViewController.swift
//  STEELIX
//
//  Created by My Mac on 30/02/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit

class JoinAsViewController: UIViewController {
   var  tag = ""
    var DisDealID:String = ""
    @IBOutlet weak var MiddleView1: UIView!
    @IBOutlet weak var TopView: UIView!
    
    @IBOutlet weak var MiddleView2: UIView!
    @IBOutlet weak var btnDealer: UIButton!
    @IBOutlet weak var dealerImg: UIImageView!
    
    @IBOutlet weak var Distributorimg: UIImageView!
    
    @IBOutlet weak var btnDistributor: UIButton!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        btnDealer.roundedAllCorner()
        btnDistributor.roundedAllCorner()
        
        dealerImg?.layer.cornerRadius = (dealerImg?.frame.size.width ?? 0.0) / 2
        dealerImg?.clipsToBounds = true
        dealerImg?.layer.borderWidth = 3.0
        dealerImg?.layer.borderColor = UIColor.white.cgColor
        
        Distributorimg?.layer.cornerRadius = (Distributorimg?.frame.size.width ?? 0.0) / 2
        Distributorimg?.clipsToBounds = true
        Distributorimg?.layer.borderWidth = 3.0
        Distributorimg?.layer.borderColor = UIColor.white.cgColor
        
      TopView.layer.cornerRadius = 30
        
        MiddleView1.layer.cornerRadius = 30
        MiddleView1.layer.borderWidth = 0.5
        MiddleView1.layer.borderColor = UIColor.black.cgColor
        
        MiddleView2.layer.cornerRadius = 30
        MiddleView2.layer.borderWidth = 0.5
        MiddleView2.layer.borderColor = UIColor.black.cgColor
        
        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnJoin(_ sender: Any) {
        DisDealID = "3"
        
        
        let second:RegistrationViewController = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
       second.id = DisDealID
        self.navigationController?.pushViewController(second, animated:true)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
   
    @IBAction func btnJoin1(_ sender: Any) {
        DisDealID = "2"
        let second:RegistrationViewController = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
        second.id = DisDealID
        self.navigationController?.pushViewController(second, animated:true)
    }
    
}
extension UIImageView {
    func roundedImage() {
       
    }
}
