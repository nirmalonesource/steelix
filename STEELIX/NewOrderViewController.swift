//
//  NewOrderViewController.swift
//  STEELIX
//
//  Created by My Mac on 02/03/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit
import iOSDropDown
import Alamofire
import SVProgressHUD

 var dicN:NSDictionary  =  [:]

class NewOrderViewController: UIViewController {

    @IBOutlet weak var arrwimg1: UIImageView!
    
    @IBOutlet weak var middleView: UIView!
    @IBOutlet weak var arrowimg: UIImageView!
    @IBOutlet weak var imgView: UIImageView!
  
    @IBOutlet weak var txtProduct:DropDown!
    @IBOutlet weak var txtQuantityindicator: UITextField!
    @IBOutlet weak var btnAddTocart: UIButton!
    @IBOutlet weak var txtSpecification: UITextField!
    @IBOutlet weak var txtCapacity: UITextField!
    @IBOutlet weak var txtName: UITextField!

    var authorization = ""
    
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var arrimg: UIImageView!
    
    @IBOutlet weak var fullspec: UIButton!
    
    @IBOutlet weak var quantity: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
     
        stepper.wraps = true
        stepper.autorepeat = true
        stepper.maximumValue = 10
        stepper.minimumValue=5
        
        
        authorization = UserDefaults.standard.string(forKey:"Token") ?? ""
        txtProduct.layer.cornerRadius = 15
        txtProduct.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        txtProduct.layer.borderWidth = 0.5
        txtProduct.layer.borderColor = UIColor.black.cgColor
    
        middleView?.layer.cornerRadius = 30
   
        middleView.layer.borderColor = UIColor.black.cgColor
        middleView.layer.borderWidth = 0.5
        imgView.layer.cornerRadius = 30
        imgView.clipsToBounds = true
        imgView.layer.borderColor = UIColor.black.cgColor
        imgView.layer.borderWidth = 0.5
        authorization = UserDefaults.standard.string(forKey:"Token") ?? ""
       self.arrwimg1.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) / 180.0)
      
        btnAddTocart.layer.cornerRadius = 20
          self.arrowimg.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) / 180.0)
        // Do any additional setup after loading the view.
        ProductList()
    }
    
    @IBAction func btnback(_ sender: Any) {
        let second:DashBoardMainViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardMainViewController") as! DashBoardMainViewController
       
        self.navigationController?.pushViewController(second, animated:true)
    }
    @IBAction func btnFullSpec(_ sender: Any) {
        
        let second:SpecificationViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpecificationViewController") as! SpecificationViewController
        second.dicS = dicN
        self.navigationController?.pushViewController(second, animated:true)
    }
    func ProductList()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            //            let headers = ["Authorization": "Basic \(base64LoginData)",
            //                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
            //                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
            //                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            //            let parameter = [
            //                "mobileno":mobileno!,
            //                "deviceid":UIDevice.current.identifierForVendor!.uuidString,
            //                "otp":OTP
            //                ] as [String : Any]
             let headers = ["Authorization":"bearer " + authorization]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.ProductList)!,
                              method: .get,
                              parameters: nil,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        // self.subjectList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            
                            let dic:NSDictionary = resData["data"] as! NSDictionary
                            let dic1:NSArray = dic["productlist"] as! NSArray
                           
                            
                            self.txtProduct.optionArray = dic1.value(forKey:"ProductName") as! [String]
                            
                            self.txtProduct.didSelect{(selectedText,index,id) in
                                self.txtProduct.text = "Selected String: \(selectedText) \n index: \(index)"
                                
                                self.fullspec.isHidden = false
                                self.arrimg.isHidden = false
                                self.middleView.isHidden = false
                        
                                let dic2 = dic1[index]
                                dicN = dic2 as! NSDictionary
                                
                                self.txtName.text = (dic2 as AnyObject).value(forKey:"ProductName") as! String
                                    self.txtCapacity.text = (dic2 as AnyObject).value(forKey:"capacity") as! String
                               
                                    self.txtSpecification.text = (dic2 as AnyObject).value(forKey:"specification") as! String
                            
                            }
                            
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                            print("invalid Otp")
                        }
                        
                        self.view.layoutIfNeeded()
                        
                        SVProgressHUD.dismiss()
                    case.failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
 
    @IBAction func steppervaluechanged(_ sender: UIStepper) {
       quantity.text=Int(sender.value).description
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
  

}
