//
//  DistributorMainDashboardViewController.swift
//  STEELIX
//
//  Created by My Mac on 02/03/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import KWVerificationCodeView
import SVProgressHUD


class DistributorDashbboardcell:UITableViewCell{
 
 
    @IBOutlet weak var lblline: UILabel!
    
    @IBOutlet weak var txtdate: UITextField!
    
    @IBOutlet weak var lblstatus: UILabel!
    
    @IBOutlet weak var uiview: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblorderno: UILabel!
    @IBOutlet weak var txtStatus: UITextField!
    @IBOutlet weak var txtOrderNo: UITextField!
    
    @IBOutlet weak var txtName: UITextField!
    
}

class DistributorMainDashboardViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tblview: UITableView!
    var arr:NSArray = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return  arr.count
   
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
     
      
        var height:CGFloat = CGFloat()
        if indexPath.row == 0 {
            height = 142
        }
        else {
            height = 50
           
        }
        
        return height
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
     
        let cell:DistributorDashbboardcell = tableView.dequeueReusableCell(withIdentifier:"cell", for: indexPath) as! DistributorDashbboardcell
     
        let orderdate = (arr[indexPath.row] as AnyObject).value(forKey: "OrderDate")
        let FirstName = (arr[indexPath.row] as AnyObject).value(forKey: "FirstName")
        let Orderno = (arr[indexPath.row] as AnyObject).value(forKey: "OrderID")
        let StatusCode = (arr[indexPath.row] as AnyObject).value(forKey: "StatusCode")
        
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd,yyyy"
        
        let date = dateFormatterGet.date(from: orderdate as! String)
        
        cell.txtdate.text=dateFormatterPrint.string(from: date!)
       
        if indexPath.row > 0
        {
            let orderdate = (arr[indexPath.row] as AnyObject).value(forKey: "OrderDate")
            let orderdate1 = (arr[indexPath.row-1] as AnyObject).value(forKey: "OrderDate")
            
            let date = dateFormatterGet.date(from: orderdate as! String)
            let date1 = dateFormatterGet.date(from: orderdate1 as! String)
            
            let date3=dateFormatterPrint.string(from: date!)
            let date4=dateFormatterPrint.string(from: date1!)
            if date3 == date4
            {
                cell.txtdate.isHidden = true
                cell.lblline.isHidden = true
                cell.lblName.isHidden = true
                cell.lblstatus.isHidden = true
                cell.lblorderno.isHidden = true
                 cell.txtOrderNo.text = Orderno as! String
                cell.txtStatus.text = StatusCode as! String
                cell.txtName.text = FirstName as! String
                 cell.uiview.frame = CGRect(x: 24,y: 10, width: 383,height:48 )
            }
            else
            {
                cell.txtdate.isHidden = false
                cell.txtdate.text = orderdate as! String
                cell.txtOrderNo.text = Orderno as! String
                cell.txtStatus.text = StatusCode as! String
                cell.txtName.text = FirstName as! String
             cell.uiview.frame = CGRect(x:29,y:102, width: 383, height:48 )
            }
        }
        else
        {
            cell.lblName.isHidden = false
            cell.lblstatus.isHidden = false
            cell.lblorderno.isHidden = false
         cell.lblline.isHidden = false
            cell.txtdate.isHidden = false
            cell.txtOrderNo.text = Orderno as! String
            cell.txtStatus.text = StatusCode as! String
            cell.txtName.text = FirstName as! String
              cell.uiview.frame = CGRect(x:29,y:102, width: 383, height:48 )
        }
       
        
//        let dateFormatter = DateFormatter()
//
//        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
//        let dateFromString : NSDate = dateFormatter.date(from: dic3 as! String)! as NSDate
//        dateFormatter.dateFormat = "dd-MM-yyyy"
//        let datenew = dateFormatter.string(from: dateFromString as Date)
//
//        cell.txtName.text = dic4 as? String
//        cell.txtOrderNo.text = dic5 as? String
//        cell.txtStatus.text = dic6 as? String
//        cell.txtdate.text = datenew as? String
//
        return cell
    }
    

    var authorization = ""
    
    @IBOutlet weak var btnReports: UIButton!
    @IBOutlet weak var btncompanyevent: UIButton!
    @IBOutlet weak var btncompanyoffer: UIButton!
    @IBOutlet weak var btnOrders: UIButton!
    @IBOutlet weak var imgview: UIImageView!
    @IBOutlet weak var topview: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
     authorization =  UserDefaults.standard.string(forKey:"Token") ?? ""
        btncompanyevent.roundedAllCorner()
        btncompanyoffer.roundedAllCorner()
        btnOrders.roundedAllCorner()
        btnReports.roundedAllCorner()
        
        topview.roundedBottom()
        imgview.roundedBottom()
        
        dealerlist()
       
        // Do any additional setup after loading the view.
    }
    
    func dealerlist()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            //            let headers = ["Authorization": "Basic \(base64LoginData)",
            //                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
            //                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
            //                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            let headers = [
              
                "Authorization": "bearer "  +  authorization
                ]
            
            let parameter = [
                
                "DistributorID" : UserDefaults.standard.string(forKey:"ID")!
                
                ] as [String : Any]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.dealerlist)!,
                              method: .post,
                              parameters: parameter,
                              headers: headers as! HTTPHeaders).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        // self.subjectList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                         let dic1 = resData["data"]
                            
                            self.arr = dic1!["dealer_list"] as! NSArray
                      
                            
                            
                            
                            self.tblview.reloadData()
                            
                        }
                            else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                        self.view.layoutIfNeeded()
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }


    @IBAction func btnOrder(_ sender: Any) {
        
        let second:OrderListViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrderListViewController") as! OrderListViewController
        
        self.navigationController?.pushViewController(second, animated:true)
    }
    @IBAction func btnReport(_ sender: Any) {
        
        let second:ReportViewController = self.storyboard?.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
        
        self.navigationController?.pushViewController(second, animated:true)
    }
    
    @IBAction func btncompanyoffer(_ sender: Any) {
        
        
        let second:CompanyOffersViewController = self.storyboard?.instantiateViewController(withIdentifier:"CompanyOffersViewController") as! CompanyOffersViewController
        self.navigationController?.pushViewController(second, animated:true)
    }
    
    @IBAction func btncompanyevent(_ sender: Any) {
        
        let second:EventViewController = self.storyboard?.instantiateViewController(withIdentifier:"EventViewController") as! EventViewController
        self.navigationController?.pushViewController(second, animated:true)
    
    }
}
