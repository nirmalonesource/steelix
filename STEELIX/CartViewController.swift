//
//  CartViewController.swift
//  STEELIX
//
//  Created by My Mac on 31/02/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit

class CartViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CartsTableViewCell=tableView.dequeueReusableCell(withIdentifier: "CartViewController", for: indexPath) as! CartsTableViewCell
        
        cell.MiddleView.roundedAllCorner()
        cell.MiddleView.layer.borderWidth=0.5
        cell.MiddleView.layer.borderColor = UIColor.black.cgColor
        return cell
        
    }
    
    

    

    
   
    
    
    @IBOutlet weak var TopView: UIView!
    
   
   
  
    
    
    @IBOutlet weak var btnPlaceOrder: UIButton!
    @IBOutlet weak var txtDistributor: UITextField!
    
   
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
       TopView.roundedBottom()
        btnPlaceOrder.roundedAllCorner()
        
        txtDistributor.layer.cornerRadius = 15
        txtDistributor.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        txtDistributor.layer.borderWidth = 0.5
        txtDistributor.layer.borderColor = UIColor.black.cgColor
        
        
        // Do any additional setup after loading the view.
    }
    
    
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
