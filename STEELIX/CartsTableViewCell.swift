//
//  CartsTableViewCell.swift
//  STEELIX
//
//  Created by My Mac on 31/02/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit

class CartsTableViewCell: UITableViewCell {

    @IBOutlet weak var txtQuantity: UITextField!
    @IBOutlet weak var txtCapacity: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var MiddleView: UIView!
    @IBOutlet weak var img1: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
