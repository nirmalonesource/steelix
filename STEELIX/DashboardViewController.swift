//
//  DashBoardViewController.swift
//  STEELIX
//
//  Created by My Mac on 01/03/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD


class DashBoardViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
  
  var authorization = ""
    var disid :Int = 0
    var disids:String = ""
    @IBOutlet weak var txtemail: UITextField!
    @IBOutlet weak var middleview: UIView!
    @IBOutlet weak var txtmobile: UITextField!
    @IBOutlet weak var profileimg: UIImageView!
    let data=["DashBoard","Support","Terms & Condition","Report"]
    

    
    let images:[UIImage]=[
        UIImage(named:"Dashboard")!,
        UIImage(named:"Support")!,
        UIImage(named: "Terms & condition")!,
        UIImage(named:"Report")!
        ]
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       disid = UserDefaults.standard.value(forKey:"ID") as! Int
        
        disids = String(disid)
    authorization =  UserDefaults.standard.string(forKey:"Token") ?? ""
        middleview.roundedRight()
     
   
        profileimg?.layer.cornerRadius = (profileimg?.frame.size.width ?? 0.0) / 2
        profileimg?.clipsToBounds = true
        profileimg?.layer.borderWidth = 3.0
        profileimg?.layer.borderColor = UIColor.white.cgColor
        
        // Do any additional setup after loading the view.
    }
  
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let second:DashBoardMainViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardMainViewController") as! DashBoardMainViewController
            self.navigationController?.pushViewController(second, animated:true)
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let cell:Dashboard1TableViewCell=tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Dashboard1TableViewCell
          cell.txtCategory.text = data[indexPath.row]
          cell.iconimg?.image = images[indexPath.row]
            return cell
        
    }
   
    @IBAction func btnlogout(_ sender: Any) {
        logout()
    }
    

    @IBAction func btneditdealer(_ sender: UIButton) {
        let second:EditViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditViewController") as! EditViewController
        self.navigationController?.pushViewController(second, animated:true)
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
    }
    func logout()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            //            let headers = ["Authorization": "Basic \(base64LoginData)",
            //                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
            //                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
            //                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            let headers = [
                
                "Authorization": "bearer "  +  authorization
            ]
            let parameter = [
                "DistributorID":disids
            ]as [String : Any]
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.logout)!,
                              method: .post,
                              parameters: parameter,
                              headers: headers as! HTTPHeaders).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        
                        
                        guard let data = response.result.value as? [String:AnyObject],
                            
                            
                            
                            
                            let _ = data["status"]! as? Bool
                            else{
                                
                                
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        // self.subjectList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            let dic =  resData["data"]
                           let second:ViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            self.navigationController?.pushViewController(second, animated:true)
                            
                        }
                            
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                        self.view.layoutIfNeeded()
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
        //Parameter HERE
        //        let parameters = [
        //            "id": "429",
        //            "docsFor" : "visitplan"
        //        ]
        //        //Header HERE
        //        let headers = [
        //            "token" : "W2Y3TUYS0RR13T3WX2X4QPRZ4ZQVWPYQ",
        //            "Content-type": "multipart/form-data",
        //            "Content-Disposition" : "form-data"
        //        ]
     
}
