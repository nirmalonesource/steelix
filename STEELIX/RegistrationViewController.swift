//
//  RegistrationViewController.swift
//  STEELIX
//
//  Created by My Mac on 30/02/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import  iOSDropDown
import KWVerificationCodeView



class RegistrationViewController: UIViewController {
  
    var userdetail:String = ""
    var Token:String = ""
    var id:String = ""
    var OTP:Int=0
    @IBOutlet weak var txtCountry: DropDown!
    
    @IBOutlet weak var btnconfirm: UIButton!
   
    @IBOutlet weak var txtState: DropDown!
    var StateID:Int = 0
     var CityID:Int = 0
   var tag = ""
    @IBOutlet weak var MiddleView: UIView!
    @IBOutlet weak var TopView: UIView!
    @IBOutlet weak var btnSendAgain: UIButton!
    @IBOutlet weak var txtotp: KWVerificationCodeView!
   
    
    
   
  
    @IBOutlet weak var txtLastname: UITextField!
    @IBOutlet weak var txtFirstnam: UITextField!
    @IBOutlet weak var btnJoin: UIButton!
    @IBOutlet weak var txtAddress: UITextField!
  
    
    @IBOutlet weak var txtregview: UIView!
    @IBOutlet weak var otpview: UIView!
    @IBOutlet weak var txtCompanyName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMob: UITextField!
 
    override func viewDidLoad() {
        super.viewDidLoad()
       
      txtregview.layer.cornerRadius = 30
        
        getState()
        MiddleView.layer.borderWidth = 0.5
        MiddleView.layer.borderColor = UIColor.black.cgColor
        TopView.layer.cornerRadius = 30
        MiddleView.layer.cornerRadius = 30
        
        btnconfirm.roundedAllCorner()
        btnSendAgain.roundedAllCorner()
        
        
//        txtName.layer.cornerRadius = 15
//        txtName.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
//        txtName.layer.borderWidth = 0.5
//        txtName.layer.borderColor = UIColor.black.cgColor
        
        txtFirstnam.layer.cornerRadius = 15
        txtFirstnam.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        txtFirstnam.layer.borderWidth = 0.5
        txtFirstnam.layer.borderColor = UIColor.black.cgColor
        
        txtLastname.layer.cornerRadius = 15
        txtLastname.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        txtLastname.layer.borderWidth = 0.5
        txtLastname.layer.borderColor = UIColor.black.cgColor
        
        txtMob.layer.cornerRadius = 15
        txtMob.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        txtMob.layer.borderWidth = 0.5
        txtMob.layer.borderColor = UIColor.black.cgColor
        
        txtEmail.layer.cornerRadius = 15
        txtEmail.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        txtEmail.layer.borderWidth = 0.5
        txtEmail.layer.borderColor = UIColor.black.cgColor
        
        txtState.layer.cornerRadius = 15
        txtState.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        txtState.layer.borderWidth = 0.5
        txtState.layer.borderColor = UIColor.black.cgColor
        
        txtCountry.layer.cornerRadius = 15
        txtCountry.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        txtCountry.layer.borderWidth = 0.5
        txtCountry.layer.borderColor = UIColor.black.cgColor
        
        txtCompanyName.layer.cornerRadius = 15
       txtCompanyName.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        txtCompanyName.layer.borderWidth = 0.5
        txtCompanyName.layer.borderColor = UIColor.black.cgColor
        
        
        
        txtAddress.layer.cornerRadius = 15
        txtAddress.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        txtAddress.layer.borderWidth = 0.5
        txtAddress.layer.borderColor = UIColor.black.cgColor
        
        btnJoin.roundedAllCorner()
        
       
//        self.arrowimg.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) / 180.0)
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnconfirm(_ sender: Any) {
         let otp:Int = Int(self.txtotp.getVerificationCode())!
        if (otp==OTP){
            RegistrationApi()

        }
        
    }
    @IBAction func btnJoin(_ sender: Any) {
       Userexistornot()
        
        
        self.TopView.isHidden=false
        self.otpview.isHidden=false
        
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        
        let second:ViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(second, animated:true)
    }
    func RegistrationApi()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
//            let headers = ["Authorization": "Basic \(base64LoginData)",
//                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
//                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
//                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            let parameter = [
          "email":txtEmail.text!,
          "CompanyName":txtCompanyName.text!,
         "FirstName":txtFirstnam.text!,
          "LastName":txtLastname.text!,
          "Address":txtAddress.text!,
         "City":CityID,
         "State":StateID,
         "roleid":id,
        "mobileno":txtMob.text!,
       "deviceid":UIDevice.current.identifierForVendor!.uuidString
                ] as [String : Any]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.Registration)!,
                              method: .post,
                              parameters: parameter,
                              headers: nil).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        
                        
                        guard let data = response.result.value as? [String:AnyObject],
                            
                            
                            
                    
                            let _ = data["status"]! as? Bool
                            else{
                                
                                
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                       // self.subjectList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                              let dic =  resData["data"]
                        //    let dic1 = dic?.value(forKey:"access_token")
                            let dic1:NSDictionary = dic?["user"] as! NSDictionary
                            
                            let dic2 =  resData["data"]
                            let dic3 = dic?.value(forKey:"access_token")
                            let dic4 = dic1.value(forKey: "id")
                            let dic5 = dic1.value(forKey: "MobileNo")
                            let dic6 = dic1.value(forKey: "FirstName")
                            let dic7 = dic1.value(forKey: "LastName")
                            let dic8 = dic1.value(forKey: "Address")
                            let dic9 = dic1.value(forKey: "EmailID")
                            let dic10 = dic1.value(forKey: "roleid")
                            let dic11 = dic1.value(forKey: "UserID")
                            
                           
                            UserDefaults.standard.set(dic10, forKey:"Roleid")
                            UserDefaults.standard.set(dic3,forKey:"Token")
                            UserDefaults.standard.set(dic4,forKey:"ID")
                            UserDefaults.standard.set(dic5, forKey: "mobileno")
                            UserDefaults.standard.set(dic6, forKey: "firstname")
                            UserDefaults.standard.set(dic7, forKey:"lastname")
                            UserDefaults.standard.set(dic8, forKey: "address")
                            UserDefaults.standard.set(dic9, forKey: "emailid")
                            UserDefaults.standard.set(dic11, forKey:"id")
                            
                            
                            
                            print( UserDefaults.standard.object(forKey: "Token") ?? "")
                            
                            
                            
                            let second:SuccessfullRegisteredViewController = self.storyboard?.instantiateViewController(withIdentifier: "SuccessfullRegisteredViewController") as! SuccessfullRegisteredViewController
                            
                           
                            self.navigationController?.pushViewController(second, animated:true)
                        }
                            
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                        self.view.layoutIfNeeded()
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    func getState()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }

        else {

            SVProgressHUD.show(withStatus: nil)

            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
           Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.getState)!,
                              method: .post,
                              parameters:nil,
                              headers: nil).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)

                        var resData : [String : AnyObject] = [:]

                        guard let data = response.result.value as? [String:AnyObject],

                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()

                                return
                        }

                        resData = data
                        // self.subjectList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                        let dic =  resData["data"]
                            let dict:NSArray = dic? ["state"] as! NSArray
                            let dic2 = dict.value(forKey: "statename")
                           let dic3 = dict.value(forKey:"stateid") as! NSArray
                        
                        
                            self.txtState.didSelect{(selectedText,index,id) in
                                self.txtState.text = "Selected String: \(selectedText) \n index: \(index)"
                                
                                self.StateID = Int(dic3.object(at: index) as! Int)
                                
                                self.getCity()
                                
                            }
                    
                           
                            self.txtState.optionArray = dic2 as! [String]
                     
                        }

                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }

                        self.view.layoutIfNeeded()

                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
   }
    func getCity()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
           
                let parameter = [
                            "stateid" : StateID
                            ] as [String : Any]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.getCity)!,
                              method: .post,
                              parameters:parameter,
                              headers: nil).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        
                        guard let data = response.result.value as? [String:AnyObject],
                            
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        // self.subjectList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            let dic =  resData["data"]
                            let dict:NSArray = dic? ["cities"] as! NSArray
                            let dic2 = dict.value(forKey: "citiesname")
                            let dic3 = dict.value(forKey: "citiesid") as! NSArray
                          
                           
                            
                            self.txtCountry.didSelect{(selectedText,index,id) in
                                self.txtCountry.text = "Selected String: \(selectedText) \n index: \(index)"
                                
                                
                                self.CityID = Int(dic3.object(at:index) as! Int)
                                print(self.StateID)
                           
                          
                            }
                              self.txtCountry.optionArray = dic2 as! [String]
                        }
                            
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                        self.view.layoutIfNeeded()
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
        
    
    func Userexistornot()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let parameter = [
                "mobileno":txtMob.text!
                ] as [String : Any]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.userExistorNot)!,
                              method: .post,
                              parameters: parameter,
                              headers: nil).responseJSON
                { (response:DataResponse) in
//            Alamofire.request((ServiceList.SERVICE_URL+ServiceList.userExistorNot), method: .post, parameters: parameter, encoding:URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
//                print(response.result)
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                    
                        resData = data
                        
                        // self.subjectList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            let dic:NSDictionary = resData["data"] as! NSDictionary
                            
                            self.OTP = dic["otp"] as! Int
                          
                            
                            }
                        
                            
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                        self.view.layoutIfNeeded()
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    @IBAction func btnResend(_ sender: Any) {
        resend()
        
    }
    func resend()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            //            let headers = ["Authorization": "Basic \(base64LoginData)",
            //                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
            //                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
            //                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            let parameter = [
                "mobileno":txtMob.text!,
                "deviceid":UIDevice.current.identifierForVendor!.uuidString,
                
                ] as [String : Any]
            
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.resend)!,
                              method: .post,
                              parameters: parameter,
                              headers: nil).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        // self.subjectList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            
                            let dic:NSDictionary = resData["data"] as! NSDictionary
                            self.OTP = dic.value(forKey: "otp") as! Int
                            
                            
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                            print("invalid Otp")
                        }
                        
                        self.view.layoutIfNeeded()
                        
                        SVProgressHUD.dismiss()
                    case.failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
}
