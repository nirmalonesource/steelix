//
//  OrderListViewController.swift
//  STEELIX
//
//  Created by My Mac on 31/02/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import KWVerificationCodeView
import SVProgressHUD


class OrderListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    var arr:NSArray = []
    var authorization:String = ""
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell :OrderListTableViewCell = tableView.dequeueReusableCell(withIdentifier:"cell", for: indexPath) as! OrderListTableViewCell
        cell.MiddleView.layer.cornerRadius = 25
        cell.MiddleView.layer.borderWidth = 0.5
        cell.MiddleView.layer.borderColor = UIColor.black.cgColor
        cell.btnDate.roundedRight()
        cell.btnStatus.roundedAllCorner()
        
       
         let orderdate = (arr[indexPath.row] as AnyObject).value(forKey: "OrderDate")
        let FirstName:String = (arr[indexPath.row] as AnyObject).value(forKey: "FirstName") as! String
        let LastName:String = (arr[indexPath.row] as AnyObject).value(forKey: "LastName") as! String
        let Orderno = (arr[indexPath.row] as AnyObject).value(forKey: "OrderID")
        let StatusCode = (arr[indexPath.row] as AnyObject).value(forKey:"StatusCode")
        let quantity = (arr[indexPath.row] as AnyObject).value(forKey: "totalQty")
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd,yyyy"
        
        let date = dateFormatterGet.date(from: orderdate as! String)
     //   let date1 = dateFormatterGet.date(from: orderdate1 as! String)
        
         let date3=dateFormatterPrint.string(from: date!)
       //let date4=dateFormatterPrint.string(from: date1!)
        
       cell.btnDate.text=dateFormatterPrint.string(from: date!)
        
        
        if indexPath.row > 0
        {
            let orderdate = (arr[indexPath.row] as AnyObject).value(forKey: "OrderDate")
            let orderdate1 = (arr[indexPath.row-1] as AnyObject).value(forKey: "OrderDate")
            
            let date = dateFormatterGet.date(from: orderdate as! String)
            let date1 = dateFormatterGet.date(from: orderdate1 as! String)
            
            let date3=dateFormatterPrint.string(from: date!)
            let date4=dateFormatterPrint.string(from: date1!)
            
            
//            let date1 = (arr[indexPath.row-0] as AnyObject).value(forKey: "OrderDate") as! String
//            let date2 = (arr[indexPath.row] as AnyObject).value(forKey: "OrderDate") as! String
            if date3 == date4
            {
               cell.btnDate.isHidden = true
                cell.lblline.isHidden = true
//                cell.lblName.isHidden = true
//                cell.lblstatus.isHidden = true
//                cell.lblorderno.isHidden = true
              
                cell.txtorderNo.text = Orderno as? String
                cell.btnStatus.text = StatusCode as? String
                cell.txtName.text = (FirstName + LastName)
                cell.txtDelieveredBy.text = quantity as? String
               // cell.uiview.frame = CGRect(x: 24,y: 10, width: 383,height:48 )
            }
            else
            {
              
                cell.btnDate.isHidden = false
          //      cell.btnDate.text=dateFormatterPrint.string(from: date!)
                
                cell.txtorderNo.text = Orderno as? String
                cell.btnStatus.text = StatusCode as? String
                cell.txtName.text = (FirstName + LastName)
                cell.txtDelieveredBy.text = quantity as? String
               // cell.uiview.frame = CGRect(x:29,y:102, width: 383, height:48 )
            }
        }
        else
        {
//            cell.lblline.isHidden = false
//          cell.btnDate.isHidden = false
//            cell.lblorderno.isHidden = false
             cell.lblline.isHidden = false
               cell.btnDate.isHidden  = false
          
            
            
            
            cell.txtorderNo.text = Orderno as? String
            cell.btnStatus.text = StatusCode as? String
            cell.txtName.text = (FirstName + LastName)
            cell.txtDelieveredBy.text = quantity as? String
           // cell.uiview.frame = CGRect(x:29,y:102, width: 383, height:48 )
        }
        
        
        
        
        
//        cell.btnDate.text=dateFormatterPrint.string(from: date!)
//        cell.txtorderNo.text = Orderno as? String
//       // cell.txtDelieveredBy.text=
//        cell.txtName.text = FirstName as? String
    
        
        
       
        
        
        
        return cell
        
        
    }
    

  
    @IBAction func btnBack(_ sender: Any) {
        let second:DistributorMainDashboardViewController = self.storyboard?.instantiateViewController(withIdentifier: "DistributorMainDashboardViewController") as!
        DistributorMainDashboardViewController
        self.navigationController?.pushViewController(second, animated:true)
      
    }
    override func viewDidLoad() {
        super.viewDidLoad()
         authorization =  UserDefaults.standard.string(forKey:"Token") ?? ""
        // Do any additional setup after loading the view.
        
        dealerlist()
    }
    func dealerlist()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
          
            
            let headers = [
                
                "Authorization": "bearer "  +  authorization
            ]
            
            let parameter = [
                
                "DistributorID" : UserDefaults.standard.string(forKey:"ID")!,
                "week" : "1"
                
                ] as [String : Any]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.dealerlist)!,
                              method: .post,
                              parameters: parameter,
                              headers: headers as! HTTPHeaders).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        // self.subjectList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            let dic1 = resData["data"]
                            
                            self.arr = dic1!["dealer_list"] as! NSArray
                            
                            
                            
                            
                            self.tblView.reloadData()
                            
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                        self.view.layoutIfNeeded()
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
