//
//  ConfirmregisterViewController.swift
//  STEELIX
//
//  Created by My Mac on 30/02/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit

class ConfirmregisterViewController: UIViewController {

    @IBOutlet weak var TopView: UIView!
    @IBOutlet weak var btnSendAgain: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var MiddleView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TopView.layer.cornerRadius = 30
        
        MiddleView.layer.cornerRadius = 30
        MiddleView.layer.borderWidth=0.5
        MiddleView.layer.borderColor = UIColor.black.cgColor
        
        btnConfirm.roundedAllCorner()
        btnSendAgain.roundedAllCorner()
        // Do any additional setup after loading the view.
    }
    

    /*
     @IBAction func btnBAck(_ sender: Any) {
     }
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnBAck(_ sender: Any) {
        let second:RegistrationViewController = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
        self.navigationController?.pushViewController(second, animated:true)
        
    }
    @IBAction func btnConfirm(_ sender: Any) {
        let second:SuccessfullRegisteredViewController = self.storyboard?.instantiateViewController(withIdentifier: "SuccessfullRegisteredViewController") as! SuccessfullRegisteredViewController
        self.navigationController?.pushViewController(second, animated:true)
    }
    
}
