//
//  EditViewController.swift
//  STEELIX
//
//  Created by My Mac on 31/02/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD


class EditViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var imageView: UIImageView!
    var imagePicker: UIImagePickerController!
    var img = UIImage()
    @IBOutlet weak var txtLastname: UITextField!
    var m = UserDefaults.standard.string(forKey: "mobileno")
    var f = UserDefaults.standard.string(forKey: "firstname")
    var l = UserDefaults.standard.string(forKey: "lastname")
    var a = UserDefaults.standard.string(forKey: "address")
    var e = UserDefaults.standard.string(forKey: "emailid")
    var r = UserDefaults.standard.string(forKey: "Roleid")
    var usd = UserDefaults.standard.string(forKey:"id")
    
    
    var authorization =  UserDefaults.standard.string(forKey:"Token") ?? ""
    
    
    @IBOutlet weak var TopView: UIView!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtDealerDistributor: UITextField!
    @IBOutlet weak var txtEmailID: UITextField!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    

      
        txtName.text = f
        txtLastname.text =  l
        txtMobileNo.text = m
        txtAddress.text = a
        txtEmailID.text = e
        
        
        TopView.roundedBottom()
        btnUpdate.roundedAllCorner()
        
        txtName.layer.cornerRadius = 15
        txtName.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        txtName.layer.borderWidth = 0.5
        txtName.layer.borderColor = UIColor.black.cgColor

        txtMobileNo.layer.cornerRadius = 15
        txtMobileNo.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        txtMobileNo.layer.borderWidth = 0.5
        txtMobileNo.layer.borderColor = UIColor.black.cgColor

        txtEmailID.layer.cornerRadius = 15
        txtEmailID.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        txtEmailID.layer.borderWidth = 0.5
        txtEmailID.layer.borderColor = UIColor.black.cgColor

        txtAddress.layer.cornerRadius = 15
        txtAddress.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        txtAddress.layer.borderWidth = 0.5
        txtAddress.layer.borderColor = UIColor.black.cgColor

        txtDealerDistributor.layer.cornerRadius = 15
        txtDealerDistributor.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        txtDealerDistributor.layer.borderWidth = 0.5
        txtDealerDistributor.layer.borderColor = UIColor.black.cgColor
        if r=="2"{
            
            txtDealerDistributor.text = "Dealer"
        }
        else{
            txtDealerDistributor.text = "Distributor"
        }
        

        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnUpdate(_ sender: Any) {
        if r=="2"{
             UPLOD()
          
        }else{
          
            UPLOD()
        }
       
    }
//    func dealerupdate()
//    {
//        if !isInternetAvailable(){
//            noInternetConnectionAlert(uiview: self)
//        }
//            
//        else {
//            
//            SVProgressHUD.show(withStatus: nil)
//            
//            let username = ServiceList.USERNAME
//            let password = ServiceList.PASSWORD
//            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
//          //  let imgData = UIImageJPEGRepresentation(img, 0.2)!
//           // let imgData = UIImage.jpegData(img)
//
//            
//            let jpegData = img.jpegData(compressionQuality: 1.0)
//           // let pngData = image.pngData()
//            
//            let parameter = [
//                
//                "UserId":usd!,
//                "FirstName":txtName.text!,
//                "LastName":txtLastname.text!,
//                "email":txtEmailID.text!,
//                "Address" :txtAddress.text!,
//                
//                
//                ] as [String : Any]
//            
//          
//
//            
////            Alamofire.upload(multipartFormData:{ multipartFormData in
////                multipartFormData.append(jpegData!, withName: "Profile")
////               },
////                             usingThreshold:UInt64.init(),
////                             to:(ServiceList.SERVICE_URL+ServiceList.update),
////                             method:.post,
////                             parameters:parameter,
////                             headers:["Authorization":"bearer " + authorization],
////                             encodingCompletion: { encodingResult in
////                                switch encodingResult {
////                                 case .success(let upload,_,_):
////                                    upload.responseJSON { response in
////                                        debugPrint(response.result.value as Any)
////                                    }
////                                case .failure(let encodingError):
////                                    print(encodingError)
////                                }
////            })
//       
////            let headers = ["Authorization":"bearer " + authorization]
////            let parameter = [
////
////                "UserId":usd!,
////                "FirstName":txtName.text!,
////                "LastName":txtLastname.text!,
////                "email":txtEmailID.text!,
////                "Address" :txtAddress.text!,
////
////
////                ] as [String : Any]
////
////            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.update)!,
////                              method: .post,
////                              parameters:parameter,
////                              headers:headers as? HTTPHeaders).responseJSON
////                { (response:DataResponse) in
////
//////                    Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.update)!, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: headers as? HTTPHeaders).responseJSON
//////                        { (response:DataResponse) in
////                            switch(response.result) {
////                            case .success(let data):
////                                print(" i got my Data Yup..",data)
////
////                                var resData : [String : AnyObject] = [:]
////
////                                guard let data = response.result.value as? [String:AnyObject],
////
////                                    let _ = data["status"]! as? Bool
////                                    else{
////                                        print("Malformed data received from fetchAllRooms service")
////                                        SVProgressHUD.dismiss()
////
////                                        return
////                                }
////
////                                resData = data
////
////                                // self.subjectList.removeAll()
////                                if resData["status"] as? Bool ?? false
////                                {
////                                    //                            let dic =  resData["data"]
////                                    //                            self.arr1 = dic?["distributo_list"] as! NSArray
////                                    //
////                                    //                            self.tblview.reloadData()
////                                    //
////
////
////
////
////                                }
////
////                                else
////                                {
////                                    showToast(uiview: self, msg: resData["message"] as? String ?? "")
////                                }
////
////                                self.view.layoutIfNeeded()
////
////                                SVProgressHUD.dismiss()
////                            case .failure(let error):
////                                print(error)
////                                SVProgressHUD.dismiss()
////                            }
////                    }
//            }
//        }
        
    @IBAction func openGallery(_ sender: Any) {
        
        
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePicker.allowsEditing = true
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                self.present(imagePicker, animated: true, completion: nil)
            }
            else
            {
                let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey :Any]) {
        img = (info[.originalImage] as? UIImage)!
        imageView.image = info[.originalImage] as? UIImage
        
        self.dismiss(animated: true, completion: nil)
        //        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
    func UPLOD()
    {
    
        let headers = ["Authorization":"bearer " + authorization]
                    let parameter = [
        
                        "UserId":usd!,
                        "FirstName":txtName.text!,
                        "LastName":txtLastname.text!,
                        "email":txtEmailID.text!,
                        "Address" :txtAddress.text!,
                    
        
                        ] as [String : Any]
        
        
   
        let jpegData = img.jpegData(compressionQuality: 0.5)
        Alamofire.upload(multipartFormData: { multipartFormData in
            //Parameter for Upload files
            multipartFormData.append(jpegData!, withName:"Profile",fileName:"furkan.png" , mimeType: "image/png")
            
            for (key, value) in parameter
            {
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
            }
            
        }, usingThreshold:UInt64.init(),
           to: ServiceList.SERVICE_URL+ServiceList.update, //URL Here
            method: .post,
            headers: headers,
            encodingCompletion: { (result) in
                
                switch result {
                case .success(let upload, _, _):
                    print("the status code is :")
                    
                    upload.uploadProgress(closure: { (progress) in
                        print("something")
                    })
                    
                    upload.responseJSON { response in
                        print("the resopnse code is : \(String(describing: response.response?.statusCode ?? nil))")
                        print("the response is : \(response)")
                        
                      
                        
                        if (self.r=="2"){
                        let second:DistributorMainDashboardViewController = self.storyboard?.instantiateViewController(withIdentifier: "DistributorMainDashboardViewController") as! DistributorMainDashboardViewController
                        self.navigationController?.pushViewController(second, animated:true)
                        }
                        else{
                            let second:DashBoardMainViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardMainViewController") as! DashBoardMainViewController
                            self.navigationController?.pushViewController(second, animated:true)
                        }
                    }
                    break
                case .failure(let encodingError):
                    print("the error is  : \(encodingError.localizedDescription)")
                    break
                }
        })
    }

    @IBAction func btback(_ sender: Any) {
        if r == "2"{
            let second:DistributorMainDashboardViewController = self.storyboard?.instantiateViewController(withIdentifier: "DistributorMainDashboardViewController") as! DistributorMainDashboardViewController
            self.navigationController?.pushViewController(second, animated:true)
        }
        else{
            let second:DashBoardMainViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardMainViewController") as! DashBoardMainViewController
            self.navigationController?.pushViewController(second, animated:true)
        }
    }
}



