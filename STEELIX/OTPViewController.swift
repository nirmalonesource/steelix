//
//  OTPViewController.swift
//  STEELIX
//
//  Created by My Mac on 30/02/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import KWVerificationCodeView
import SVProgressHUD




class OTPViewController: UIViewController {
    var roleid:Int = 0
    var userdetail:String = ""
    var OTP :Int = 0
    var otpentered :String!
    var mobileno:String!
    var accesstoken:String!
    
    @IBOutlet weak var MiddleView: UIView!
    @IBOutlet weak var TopView: UIView!
    @IBOutlet weak var btnSendAgain: UIButton!
    @IBOutlet weak var txtotp: KWVerificationCodeView!
    @IBOutlet weak var btnconfirm: UIButton!
 
   
    override func viewDidLoad() {
        super.viewDidLoad()
        MiddleView.layer.borderWidth = 0.5
        MiddleView.layer.borderColor = UIColor.black.cgColor
        TopView.layer.cornerRadius = 30
        MiddleView.layer.cornerRadius = 30
        
        btnconfirm.roundedAllCorner()
        btnSendAgain.roundedAllCorner()
        
      
        
        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnConfirm(_ sender: Any) {
    
        
       otpconfirm()
        

}
    func otpconfirm()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            //            let headers = ["Authorization": "Basic \(base64LoginData)",
            //                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
            //                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
            //                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            let parameter = [
                "mobileno":mobileno!,
                "deviceid":UIDevice.current.identifierForVendor!.uuidString,
                "otp":OTP
                ] as [String : Any]
            
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.otpconfirm)!,
                              method: .post,
                              parameters: parameter,
                              headers: nil).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        // self.subjectList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            let dic:NSDictionary = resData["data"] as! NSDictionary
                            let dic1:NSDictionary = dic["user"] as! NSDictionary
                           
                            let dic2 =  resData["data"]
                            let dic3 = dic.value(forKey:"access_token")
                            let dic4 = dic1.value(forKey: "id")
                            let dic5 = dic1.value(forKey: "MobileNo")
                            let dic6 = dic1.value(forKey: "FirstName")
                            let dic7 = dic1.value(forKey: "LastName")
                            let dic8 = dic1.value(forKey: "Address")
                            let dic9 = dic1.value(forKey: "EmailID")
                            let dic10 = dic1.value(forKey: "roleid")
                            let dic11 = dic1.value(forKey: "UserID")
                            
                            
                            UserDefaults.standard.set(dic10, forKey:"Roleid")
                            UserDefaults.standard.set(dic3,forKey:"Token")
                            UserDefaults.standard.set(dic4,forKey:"ID")
                            UserDefaults.standard.set(dic5, forKey: "mobileno")
                            UserDefaults.standard.set(dic6, forKey: "firstname")
                            UserDefaults.standard.set(dic7, forKey:"lastname")
                            UserDefaults.standard.set(dic8, forKey: "address")
                            UserDefaults.standard.set(dic9, forKey: "emailid")
                            UserDefaults.standard.set(dic11, forKey:"id")
                            
                            
                            self.roleid = dic1.value(forKey: "roleid")! as! Int
                           let otp:Int = Int(self.txtotp.getVerificationCode())!
                           
                            if(otp==self.OTP)
                        {
                            if (self.roleid==3){
                            let second:DashBoardMainViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardMainViewController") as! DashBoardMainViewController
                            
                          //  second.Roleid = roleid as! Int
                            self.navigationController?.pushViewController(second, animated: true)
                            
                            }else{
                                let second:DistributorMainDashboardViewController = self.storyboard?.instantiateViewController(withIdentifier: "DistributorMainDashboardViewController") as! DistributorMainDashboardViewController
                                
                                //  second.Roleid = roleid as! Int
                                self.navigationController?.pushViewController(second, animated: true)
                            }
                            }
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                            print("invalid Otp")
                        }
                        
                        self.view.layoutIfNeeded()
                        
                        SVProgressHUD.dismiss()
                    case.failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    @IBAction func btnsendagain(_
        sender: Any) {
        resend()
        
    }
    func resend()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            //            let headers = ["Authorization": "Basic \(base64LoginData)",
            //                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
            //                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
            //                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            let parameter = [
                "mobileno":mobileno!,
                "deviceid":UIDevice.current.identifierForVendor!.uuidString,
                
                ] as [String : Any]
            
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.resend)!,
                              method: .post,
                              parameters: parameter,
                              headers: nil).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        // self.subjectList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            let dic:NSDictionary = resData["data"] as! NSDictionary
                            
                            
                            self.OTP = dic.value(forKey: "otp") as! Int
//                            let dic:NSDictionary = resData["data"] as! NSDictionary
//                            let dic1:NSDictionary = dic["user"] as! NSDictionary
//
//                            let dic2 =  resData["data"]
//                            let dic3 = dic.value(forKey:"access_token")
//                            let dic4 = dic1.value(forKey: "id")
//                            let dic5 = dic1.value(forKey: "MobileNo")
//                            let dic6 = dic1.value(forKey: "FirstName")
//                            let dic7 = dic1.value(forKey: "LastName")
//                            let dic8 = dic1.value(forKey: "Address")
//                            let dic9 = dic1.value(forKey: "EmailID")
//                            let dic10 = dic1.value(forKey: "roleid")
//                            let dic11 = dic1.value(forKey: "UserID")
//
//
//                            UserDefaults.standard.set(dic10, forKey:"Roleid")
//                            UserDefaults.standard.set(dic3,forKey:"Token")
//                            UserDefaults.standard.set(dic4,forKey:"ID")
//                            UserDefaults.standard.set(dic5, forKey: "mobileno")
//                            UserDefaults.standard.set(dic6, forKey: "firstname")
//                            UserDefaults.standard.set(dic7, forKey:"lastname")
//                            UserDefaults.standard.set(dic8, forKey: "address")
//                            UserDefaults.standard.set(dic9, forKey: "emailid")
//                            UserDefaults.standard.set(dic11, forKey:"id")
//
//
//                            self.roleid = dic1.value(forKey: "roleid")! as! Int
//                            let otp:Int = Int(self.txtotp.getVerificationCode())!
//
//                            if(otp==self.OTP)
//                            {
//                                if (self.roleid==3){
//                                    let second:DashBoardMainViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardMainViewController") as! DashBoardMainViewController
//
//                                    //  second.Roleid = roleid as! Int
//                                    self.navigationController?.pushViewController(second, animated: true)
//
//                                }else{
//                                    let second:DistributorMainDashboardViewController = self.storyboard?.instantiateViewController(withIdentifier: "DistributorMainDashboardViewController") as! DistributorMainDashboardViewController
//
//                                    //  second.Roleid = roleid as! Int
//                                    self.navigationController?.pushViewController(second, animated: true)
//                                }
//                            }
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                            print("invalid Otp")
                        }
                        
                        self.view.layoutIfNeeded()
                        
                        SVProgressHUD.dismiss()
                    case.failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
}
