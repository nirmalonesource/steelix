//
//  ViewController.swift
//  STEELIX
//
//  Created by My Mac on 30/02/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class ViewController: UIViewController {
    var OTP:Int = 0
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var btnDistributor: UIButton!
    @IBOutlet weak var btnDealer: UIButton!
    @IBOutlet weak var MiddleView: UIView!
    @IBOutlet weak var topView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
     
       
    topView.roundedBottom()
        
     MiddleView.layer.borderWidth = 0.5
//    MiddleView.clipsToBounds = true
      MiddleView.layer.borderColor = UIColor.black.cgColor
        MiddleView.layer.cornerRadius  = 30
//        MiddleView.roundedAllCorner()
       
        btnDealer.roundedLeft()
        
        txtMobileNo.setBottomBorder()
       btnNext.roundedAllCorner()
       
      btnDistributor.layer.borderColor = UIColor.black.cgColor
//        btnDistributor.roundedRight()
        btnDistributor.layer.cornerRadius = 30
        btnDistributor.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
        btnDistributor.layer.borderWidth = 0.5
        
       // btnDistributor.mask?.bounds
       
    }

    @IBAction func btnRegister(_ sender: Any) {
     let second:JoinAsViewController = self.storyboard?.instantiateViewController(withIdentifier: "JoinAsViewController") as! JoinAsViewController
        self.navigationController?.pushViewController(second,animated: true)
    }
    
    @IBAction func btnNext(_ sender: Any) {
        Login()
       
        
        
        
    }
    func Login()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            //            let headers = ["Authorization": "Basic \(base64LoginData)",
            //                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
            //                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
            //                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            let parameter = [
                "mobileno":txtMobileNo.text!,
                "deviceid":UIDevice.current.identifierForVendor!.uuidString
                ] as [String : Any]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.Login)!,
                              method: .post,
                              parameters: parameter,
                              headers: nil).responseJSON
                  { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        // self.subjectList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            
                            //                            let second:OTPViewController = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                            //                            self.navigationController?.pushViewController(second, animated:true)
                            //                            second.otpentered =  OTP
                            let second:OTPViewController = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                            
                            let dic:NSDictionary = resData["data"] as! NSDictionary
                            
                            self.OTP = dic["otp"] as! Int
                            second.mobileno  = self.txtMobileNo.text
                            second.OTP = self.OTP
                            self.navigationController?.pushViewController(second, animated:true)
                        }
                            
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                        self.view.layoutIfNeeded()
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
}

