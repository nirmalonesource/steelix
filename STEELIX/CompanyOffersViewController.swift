//
//  CompanyOffersViewController.swift
//  STEELIX
//
//  Created by My Mac on 02/03/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD


class companyoffer:UITableViewCell{
    
  
    @IBOutlet weak var middleView: UIView!
    
    
    @IBOutlet weak var imgview1: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
 
    
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
       middleView.layer.cornerRadius = 30
        middleView.layer.borderColor = UIColor.black.cgColor
        middleView.layer.borderWidth = 0.5
        
        // Configure the view for the selected state
    }
    
}

class CompanyOffersViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var s:String = ""
    var roleid :String = ""
    
    
    @IBOutlet weak var tblview: UITableView!
    
     var authorization =  UserDefaults.standard.string(forKey:"Token") ?? ""
    
       var arr1:NSArray = []
    let images:[UIImage]=[
        UIImage(named:"Dashboard")!,
        UIImage(named:"Support")!,
        UIImage(named: "Terms & condition")!,
        UIImage(named:"Report")!
        
    ]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return arr1.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:companyoffer=tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! companyoffer
        
        
        s = ((arr1.object(at:indexPath.row)) as AnyObject).value(forKey:"offerImageUrl") as! String
        let url = NSURL(string:s)
        let data = NSData(contentsOf:url! as URL)
        cell.imgview1.image = UIImage(data:data! as Data)
        return cell
    }
    
    @IBAction func btnback(_ sender: Any) {
        if roleid == "2"{
            let second:DistributorMainDashboardViewController = self.storyboard?.instantiateViewController(withIdentifier: "DistributorMainDashboardViewController") as! DistributorMainDashboardViewController
            self.navigationController?.pushViewController(second, animated:true)
        }
        else{
            let second:DashBoardMainViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardMainViewController") as! DashBoardMainViewController
            self.navigationController?.pushViewController(second, animated:true)
        }
    }
    
    @IBOutlet weak var Topview: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Topview.roundedBottom()
        offer()
         roleid =  UserDefaults.standard.string(forKey: "Roleid")!
        // Do any additional setup after loading the view.
    }
    
    func offer()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            //            let headers = ["Authorization": "Basic \(base64LoginData)",
            //                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
            //                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
            //                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            //            let parameter = [
            //                "mobileno":mobileno!,
            //                "deviceid":UIDevice.current.identifierForVendor!.uuidString,
            //                "otp":OTP
            //                ] as [String : Any]
            let headers = ["Authorization":"bearer " + authorization]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.offer)!,
                              method: .post,
                              parameters:nil,
                              headers: headers as? HTTPHeaders).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        // self.subjectList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            
                            let dic:NSDictionary = resData["data"] as! NSDictionary
                            self.arr1 = dic["offer_list"] as! NSArray
                            
                            self.tblview.reloadData()
                            
                            
                         
                            
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                            print("invalid Otp")
                        }
                        
                        self.view.layoutIfNeeded()
                        
                        SVProgressHUD.dismiss()
                    case.failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
