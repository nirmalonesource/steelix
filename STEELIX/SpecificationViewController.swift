//
//  SpecificationViewController.swift
//  STEELIX
//
//  Created by My Mac on 06/03/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit
import iOSDropDown
import Alamofire
import SVProgressHUD



class SpecificationViewController: UIViewController {
 var authorization = ""
    var dicS:NSDictionary  =  [:]
    var s:String=""
   
   
    @IBOutlet weak var btnfullspec: UILabel!
    @IBOutlet weak var topview: UIView!
    @IBOutlet weak var txtKeyFeature: UITextField!
    @IBOutlet weak var txtCapacity: UITextField!
    @IBOutlet weak var txtSeries: UITextField!
    @IBOutlet weak var txtProduct: UITextField!
    @IBOutlet weak var imgView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
    
    let arr1:NSArray = dicS["product_image"] as! NSArray
      
        self.txtProduct.text =  dicS.value(forKey: "ProductName") as! String
        self.txtSeries.text = dicS.value(forKey: "seriesname") as! String
        self.txtCapacity.text=dicS.value(forKey: "capacity") as! String
        self.txtKeyFeature.text = dicS.value(forKey: "keyfeature") as! String
     
        s = ((arr1.object(at: 0)) as AnyObject).value(forKey: "image") as! String
        let url = NSURL(string:s)
        let data = NSData(contentsOf:url! as URL)
        imgView.image = UIImage(data:data! as Data)
     
        
        topview.roundedBottom()
        btnfullspec.roundedRight()
        // Do any additional setup after loading the view.
    }
    
   

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
    @IBAction func btnback(_ sender: Any) {
        let second:NewOrderViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewOrderViewController") as! NewOrderViewController
        self.navigationController?.pushViewController(second, animated:true)
    }
    }

