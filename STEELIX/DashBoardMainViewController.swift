//
//  DashBoardMainViewController.swift
//  STEELIX
//
//  Created by My Mac on 01/03/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import  iOSDropDown
import KWVerificationCodeView


class Dashboardcell:UITableViewCell{
    
   
    @IBOutlet weak var txtcompanyname: UITextField!
    
    @IBOutlet weak var txtemail: UITextField!
    
    @IBOutlet weak var txtmobile: UITextField!
}

class DashBoardMainViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
     var arr1:NSArray = []
    var ID = UserDefaults.standard.string(forKey:"ID")
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return arr1.count
    }
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:Dashboardcell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Dashboardcell
          tableView.separatorColor = UIColor.darkGray
         let dict:NSDictionary = arr1.object(at: indexPath.row) as! NSDictionary
        cell.txtcompanyname!.text = dict.value(forKey:"CompanyName") as? String
        print(cell.txtcompanyname)
        cell.txtemail!.text = dict.value(forKey:"EmailID") as? String
        print(cell.txtemail.text!)
        
        cell.txtmobile!.text = dict.value(forKey:"MobileNo") as? String
        return cell
    }
    
    @IBAction func btnNewOrder(_ sender: Any) {
        
        let second:NewOrderViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewOrderViewController") as! NewOrderViewController
        
        
        self.navigationController?.pushViewController(second, animated:true)
    }
    var authorization = ""
    var roleid:String = ""
    var tokentype = "bearer"
    @IBOutlet weak var btnCompanyOffers: UIButton!
    
    @IBOutlet weak var btnCompanyEvents: UIButton!
    @IBOutlet weak var TopImgView: UIImageView!
    @IBOutlet weak var TopView: UIView!
    
    @IBOutlet weak var tblview: UITableView!
    @IBOutlet weak var lblDistributor: UILabel!
    @IBOutlet weak var btnOrderHistory: UIButton!
    @IBOutlet weak var btnDealerOrder: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        authorization =  UserDefaults.standard.string(forKey:"Token") ?? ""
        
        getDistributorList()
    btnDealerOrder.roundedAllCorner()
        btnOrderHistory.roundedAllCorner()
        
    btnCompanyEvents.roundedAllCorner()
        btnCompanyOffers.roundedAllCorner()
         TopView.roundedBottom()
        TopImgView.roundedBottom()
        
        lblDistributor.roundedRight()
    
       
       
    }
    
    @IBAction func btnorderhistory(_ sender: Any) {
//        let second:OrderListViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrderListViewController") as! OrderListViewController
//        self.navigationController?.pushViewController(second, animated:true)
        
    }
    @IBAction func btnCompanyoffersd(_ sender: Any) {
        
        let second:CompanyOffersViewController = self.storyboard?.instantiateViewController(withIdentifier: "CompanyOffersViewController") as! CompanyOffersViewController
             self.navigationController?.pushViewController(second, animated:true)
    }
    
    @IBAction func btnBack(_ sender: Any) {
      
     
    }
    
    @IBAction func btnCompanyevents(_
        sender: Any) {
        let second:EventViewController = self.storyboard?.instantiateViewController(withIdentifier:"EventViewController") as! EventViewController
        self.navigationController?.pushViewController(second, animated:true)
    }
    @IBAction func btnDealer(_ sender: Any) {
        
        let second:EditViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditViewController") as! EditViewController
        
        
        self.navigationController?.pushViewController(second, animated:true)
        
    }
    
    func getDistributorList()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
           
            let headers = ["Authorization":"bearer " + authorization]
            let parameter = [
                
                "id":ID
                ] as [String : Any]
            
//            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.getDistributorList)!,
//                              method: .post,
//                              parameters:nil,
//                              headers:parameter as? HTTPHeaders).responseJSON
//                { (response:DataResponse) in
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.getDistributorList)!, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: headers as? HTTPHeaders).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        
                        guard let data = response.result.value as? [String:AnyObject],
                            
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        
                        // self.subjectList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            let dic =  resData["data"]
                            self.arr1 = dic?["distributo_list"] as! NSArray
                        
                            self.tblview.reloadData()
                                
                            
                            
                           
                            
                        }
                            
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                        self.view.layoutIfNeeded()
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
     */

}
