//
//  OrderListTableViewCell.swift
//  STEELIX
//
//  Created by My Mac on 01/03/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit

class OrderListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblline: UILabel!
    @IBOutlet weak var btnDate: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtDelieveredBy: UITextField!
    @IBOutlet weak var txtorderNo: UITextField!
    @IBOutlet weak var btnStatus: UITextField!
    @IBOutlet weak var MiddleView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
