//
//  OrderViewController.swift
//  STEELIX
//
//  Created by My Mac on 31/02/1941 Saka.
//  Copyright © 1941 My Mac. All rights reserved.
//

import UIKit

class OrderViewController: UIViewController {

    @IBOutlet weak var txtdeliverydate: UITextField!
    @IBOutlet weak var btnPlaceOrder: UIButton!
    @IBOutlet weak var txtDeliveredBy: UITextField!
    @IBOutlet weak var txtTotalQuantity: UITextField!
    
    @IBOutlet weak var txtOrderNo: UITextField!
    @IBOutlet weak var txtDistributorNo: UITextField!
    @IBOutlet weak var TopView: UIView!
    @IBOutlet weak var BottomView: UIView!
    @IBOutlet weak var MiddleView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
  TopView.roundedBottom()
     MiddleView.layer.cornerRadius = 25
        BottomView.layer.cornerRadius = 25
        
        txtDistributorNo.setBottomBorder()
        btnPlaceOrder.roundedAllCorner()
         BottomView.layer.borderWidth = 0.5
        BottomView.layer.borderColor = UIColor.black.cgColor
        
        txtOrderNo.layer.borderColor = UIColor.black.cgColor
        txtOrderNo.layer.borderWidth = 0.5
        
        txtTotalQuantity.layer.borderColor = UIColor.black.cgColor
        txtTotalQuantity.layer.borderWidth = 0.5
        
        txtDeliveredBy.layer.borderColor = UIColor.black.cgColor
        txtDeliveredBy.layer.borderWidth = 0.5
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
